#include "MainWindow.h"

#include <QApplication>
#include <QFile>
#include <QDebug>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QApplication::setApplicationName("GraphViewer");
	QApplication::setOrganizationName("Chronosoft");
	QApplication::setApplicationDisplayName("Graph Theory Viewer");

	QFile styleFile(":/defaultStyle.css");
	if(styleFile.open(QIODevice::ReadOnly))
	{
		a.setStyleSheet(styleFile.readAll());
		styleFile.close();
	}
	else
		qWarning() << "Failed to load the style" << styleFile.fileName();

	MainWindow w;
	w.setStyleSheet(a.styleSheet());
	w.show();
	return a.exec();
}
