#include "LegendView.h"

#include <QLabel>
#include <QVBoxLayout>
#include <QStyleOption>
#include <QPainter>

LegendView::LegendView(QWidget *parent):
	QWidget(parent)
{
	setProperty("style", "legend");
}

void LegendView::setColors(QMap<QString, QColor> color)
{
	m_color = color;
	rebuild();
}

void LegendView::clear()
{
	m_color.clear();
}

void LegendView::rebuild()
{
	foreach(QObject* obj, this->children())
		delete obj;

	if(!m_color.isEmpty())
	{
		QVBoxLayout* layout = new QVBoxLayout;
		layout->setMargin(6);
		setLayout(layout);

		layout->addWidget(new QLabel(tr("Legend :")));

		foreach (QString author, m_color.keys())
		{
			QLabel* name = new QLabel(QString("\t<font color=\"%1\">%2</font>").arg(m_color.value(author).name()).arg(author));

			layout->addWidget(name);
		}
	}
	resize(this->width(), 12 + (m_color.size() + 1) * 20);
	update();
}

void LegendView::paintEvent(QPaintEvent* event)
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
	QWidget::paintEvent(event);
}
