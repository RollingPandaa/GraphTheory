#include "GraphicsEdge.h"

#include <QPainter>
#include <QGraphicsScene>
#include <cmath>

GraphicsEdge::GraphicsEdge(SharedEdge edge, QGraphicsItem* from, QGraphicsItem* to):
	QGraphicsItem(nullptr),
	m_edge(edge),
	m_fromNode(from),
	m_toNode(to),
	m_oriented(false)
{
	updatePosition();
}

void GraphicsEdge::setColor(QColor color)
{
	m_color = color;
}


QRectF GraphicsEdge::boundingRect() const
{
	return shape().boundingRect();
}

QPainterPath GraphicsEdge::shape() const
{
	return QGraphicsLineItem(getLine()).shape();
}

void GraphicsEdge::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	Q_UNUSED(option)
	Q_UNUSED(widget)

	updatePosition();

	QPen pen(painter->pen());
	if(m_color.isValid())
		painter->setPen(m_color);

	painter->drawLines(arrow());

	painter->setPen(pen);
}

void GraphicsEdge::updatePosition()
{
	setPos(0.5 * (m_toNode->pos() + m_fromNode->pos()));
}

QLineF GraphicsEdge::getLine() const
{
	QPainterPath thisShape(QGraphicsLineItem(QLineF(m_fromNode->pos() - pos(), m_toNode->pos() - pos())).shape());

	QPainterPath path(m_fromNode->shape());
	path.translate(m_fromNode->pos() - pos());
	thisShape = thisShape.subtracted(path);

	path = m_toNode->shape();
	path.translate(m_toNode->pos() - pos());
	thisShape = thisShape.subtracted(path);

	QLineF line;
	if(thisShape.elementCount() < 2)
		line = QLineF(pos(), pos());
	else
		line = QLineF(thisShape.elementAt(0), thisShape.elementAt(1));

	return line;
}

QVector<QLine> GraphicsEdge::arrow() const
{
	QVector<QLine> result;
	result.append(getLine().toLine());

	QLineF little(result.first().p2(), result.first().p1());
	little.setLength(scene()->font().pointSize());

	little.setAngle(little.angle() + 30);
	result.append(little.toLine());

	little.setAngle(little.angle() - 60);
	result.append(little.toLine());

	return result;
}
