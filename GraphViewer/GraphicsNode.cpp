#include "GraphicsNode.h"

#include <QPainter>
#include <QGraphicsScene>
#include <QDebug>

GraphicsNode::GraphicsNode(SharedNode node, QGraphicsItem* parent):
	QGraphicsItem(parent),
	m_node(node),
	m_textRect(0, 0, 20, 50)
{
	setFlag(ItemIsSelectable);
	setFlag(ItemIsMovable);
	setFlag(ItemSendsScenePositionChanges);
}

QUuid GraphicsNode::ident() const
{
	return m_node->ident();
}

qreal GraphicsNode::higherDimension() const
{
	return qMax(boundingRect().width(), boundingRect().height());
}


QRectF GraphicsNode::boundingRect() const
{
	return QRectF(m_textRect.adjusted(m_textRect.left() * 0.5, m_textRect.top() * 0.5, m_textRect.right() * 0.5, m_textRect.bottom() * 0.5));
}

QPainterPath GraphicsNode::shape() const
{
	return QGraphicsEllipseItem(boundingRect()).shape();
}

void GraphicsNode::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
	if(painter && m_node)
	{
		m_textRect = painter->fontMetrics().boundingRect(m_node->label.toString());
		m_textRect.moveCenter(QPointF(0, 0));
		QBrush b(painter->brush());
		QPen p(painter->pen());
		painter->setPen(isSelected() ? Qt::white : Qt::black);
		painter->setBrush(isSelected() ? QBrush(Qt::black, Qt::SolidPattern) : QBrush(Qt::white, Qt::SolidPattern));

		painter->drawEllipse(boundingRect());
		painter->drawText(m_textRect, Qt::AlignVCenter | Qt::AlignHCenter, m_node->label.toString());

		painter->setBrush(b);
		painter->setPen(p);
	}
}

QVariant GraphicsNode::itemChange(GraphicsItemChange change, const QVariant& value)
{
	if(scene() && (change == ItemPositionHasChanged || change == ItemSelectedHasChanged))
		scene()->update(scene()->sceneRect());

	return QGraphicsItem::itemChange(change, value);
}
