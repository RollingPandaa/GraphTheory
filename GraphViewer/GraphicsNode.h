#ifndef GRAPHICSNODE_H
#define GRAPHICSNODE_H

#include <QGraphicsItem>

#include "GraphElement.h"

class GraphicsNode : public QGraphicsItem
{
	public:
		GraphicsNode(SharedNode node, QGraphicsItem* parent = nullptr);

		QUuid ident() const;
		qreal higherDimension() const;

		QRectF boundingRect() const override;
		QPainterPath shape() const override;
		void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

	protected:
		QVariant itemChange(GraphicsItemChange change, const QVariant& value) override;

	private:
		SharedNode m_node;
		QRectF m_textRect;
		QPainterPath m_shape;
};

#endif // GRAPHICSNODE_H
