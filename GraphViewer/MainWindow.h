#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPointer>

#include "Graph.h"
#include "LegendView.h"

class QGraphicsView;
class GraphScene;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
		Q_OBJECT

	public:
		MainWindow(QWidget *parent = nullptr);
		~MainWindow();

		void createGraph();

	protected:
		void paintEvent(QPaintEvent* event) override;

	private slots:
		void loadNames();
		void saveGraph();
		void showMessage(QString message);

	private:
		Ui::MainWindow *ui;
		QGraphicsView* m_view;
		GraphScene* m_scene;
		LegendView* m_legend;
};
#endif // MAINWINDOW_H
