#include "ViewEventFilter.h"

#include <QWheelEvent>
#include <QMouseEvent>
#include <QKeyEvent>

ViewEventFilter::ViewEventFilter(QGraphicsView *view, QObject *parent):
	QObject(parent),
	m_view(view)
{
}


bool ViewEventFilter::eventFilter(QObject* watched, QEvent* event)
{
	if(m_view && m_view == watched)
	{
		switch(event->type())
		{
			case QEvent::Wheel:
			{
				QWheelEvent* wheel = dynamic_cast<QWheelEvent*>(event);
				if(wheel && wheel->modifiers() & Qt::CTRL)
				{
					if(wheel->angleDelta().y() > 0)
						m_view->scale(1.1, 1.1);
					else if(wheel->angleDelta().y() < 0)
						m_view->scale(0.9, 0.9);
				}
				return true;
				break;
			}

			case QEvent::KeyPress:
			case QEvent::KeyRelease:
			{
				QKeyEvent* key = dynamic_cast<QKeyEvent*>(event);
				if(key && key->key() == Qt::Key_Control)
				{
					m_view->setDragMode(key->modifiers() & Qt::CTRL ? QGraphicsView::ScrollHandDrag : QGraphicsView::RubberBandDrag);
					return true;
				}
				else
					return false;
			}

			default:
				return false;
				break;
		}
	}
	else
		return false;
}
