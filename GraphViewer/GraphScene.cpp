#include "GraphScene.h"

#include <QGraphicsProxyWidget>
#include <cmath>
#include <QSettings>
#include <QCoreApplication>
#include <QGraphicsView>
#include <QGraphicsSceneDragDropEvent>
#include <QMouseEvent>
#include <QDebug>

#include "GraphicsNode.h"
#include "GraphicsEdge.h"
#include "LegendView.h"

GraphScene::GraphScene(QObject* parent):
	QGraphicsScene(parent),
	m_graph(new Graph(true, true, true)),
	m_modulo(8),
	m_placementIndex(-1),
	m_lastColor(QColor::fromHsv(0, 255, 255)),
	m_legendWidget(nullptr)
{
	m_graph->setUniqueLabels(true);

	connect(m_graph, &Graph::nodeAdded, this, &GraphScene::addNode);
	connect(m_graph, &Graph::edgeAdded, this, &GraphScene::addEdge);
	connect(m_graph, &Graph::graphLoaded, this, &GraphScene::loadPositions, Qt::QueuedConnection);
}

void GraphScene::addColor(QVariant key)
{
	if(m_colors.isEmpty())
		m_colors.insert(key.toString(), QColor::fromHsv(0, 255, 255));
	else
	{
		if(!m_colors.contains(key.toString()))
		{
			m_lastColor.setHsv((m_lastColor.hsvHue() + 40) % 360, m_lastColor.hsvSaturation(), m_lastColor.value());
			m_colors.insert(key.toString(), m_lastColor);
			if(m_legendWidget)
				m_legendWidget->setColors(m_colors);
		}
	}
}

void GraphScene::setLegendWidget(LegendView* view)
{
	m_legendWidget = view;
	if(m_legendWidget)
		m_legendWidget->setColors(m_colors);
}

void GraphScene::clear()
{
	QGraphicsScene::clear();
	delete m_graph;
	m_graph.clear();
	m_name.clear();
	m_colors.clear();
	m_centralNode.clear();
}

void GraphScene::saveGraph(QString name)
{
	if(!name.isEmpty())
	{
		QSettings file(QSettings::IniFormat, QSettings::UserScope, QCoreApplication::organizationName(), QCoreApplication::applicationName());
		file.beginGroup(name);

		m_graph->saveGraph(file);
		file.setValue("root", m_centralNode->ident());

		file.beginWriteArray("view");
		int i(0);
		foreach(QGraphicsItem* item, items())
		{
			GraphicsNode* node = dynamic_cast<GraphicsNode*>(item);
			if(node)
			{
				file.setArrayIndex(i++);
				file.setValue("ident", node->ident());
				file.setValue("pos", item->pos());
			}
		}
		file.endArray();
		file.endGroup();

		emit informationMessage(tr("Graph \"%1\" saved").arg(name));
	}
}

void GraphScene::loadGraph(QString name)
{
	if(!name.isEmpty())
	{
		clear();

		QSettings file(QSettings::IniFormat, QSettings::UserScope, QCoreApplication::organizationName(), QCoreApplication::applicationName());
		file.beginGroup(name);

		m_graph = new Graph(this);
		connect(m_graph, &Graph::nodeAdded, this, &GraphScene::addNode);
		connect(m_graph, &Graph::edgeAdded, this, &GraphScene::addEdge);
		connect(m_graph, &Graph::graphLoaded, this, &GraphScene::loadPositions, Qt::QueuedConnection);

		m_name = name;
		m_graph->loadGraph(file);
		m_centralNode = m_graph->getNode(file.value("root").toUuid());

		file.endGroup();
	}
}

void GraphScene::addNode(SharedNode node)
{
	GraphicsNode* item = new GraphicsNode(node);
	m_items.insert(node, item);
	addItem(item);

	if(m_placementIndex >= 0)
	{
		int spire = 1 + m_placementIndex / m_modulo;
		qreal ray = spire * item->higherDimension();
		//			ray *= (m_placementIndex % 2 == 0 ? 1.1 : 0.9);
		qreal angle = qreal(m_placementIndex % m_modulo) * M_PI_4;
		if(spire % 2 == 1)
			angle += M_PI_4 * 0.5;
		item->setPos(ray * cos(angle), ray * sin(angle));
	}
	m_placementIndex += 1;
	update(sceneRect());
}

void GraphScene::addEdge(SharedEdge edge)
{
	GraphicsEdge* item = new GraphicsEdge(edge, m_items.value(m_graph->getStartNodeOf(edge)), m_items.value(m_graph->getEndNodeOf(edge)));
	item->setOriented(m_graph->isOriented());
	if(edge->label.isValid())
	{
		addColor(edge->label);
		item->setColor(m_colors.value(edge->label.toString()));
	}
	m_items.insert(edge, item);
	addItem(item);
	update(sceneRect());
}

void GraphScene::loadPositions()
{
	QSettings file(QSettings::IniFormat, QSettings::UserScope, QCoreApplication::organizationName(), QCoreApplication::applicationName());
	file.beginGroup(m_name);

	int size(file.beginReadArray("view"));
	QMap<QUuid, QPointF> data;
	for(int i = 0; i < size; ++i)
	{
		file.setArrayIndex(i);
		if(!m_graph->getNode(file.value("ident").toUuid()).isNull())
			data.insert(file.value("ident").toUuid(), file.value("pos").toPointF());
	}
	file.endArray();

	GraphicsNode* central = nullptr;
	foreach(QGraphicsItem* item, items())
	{
		GraphicsNode* node = dynamic_cast<GraphicsNode*>(item);
		if(node && data.contains(node->ident()))
		{
			node->setPos(data.value(node->ident()));
			if(node->ident() == m_centralNode->ident())
				central = node;
		}
	}
	file.endGroup();

	if(central)
		central->setSelected(true);

	update(sceneRect());
}
