#ifndef VIEWEVENTFILTER_H
#define VIEWEVENTFILTER_H

#include <QObject>
#include <QGraphicsView>
#include <QPointer>

class ViewEventFilter : public QObject
{
		Q_OBJECT
	public:
		explicit ViewEventFilter(QGraphicsView* view, QObject *parent = nullptr);

		bool eventFilter(QObject* watched, QEvent* event) override;

	private:
		QPointer<QGraphicsView> m_view;
};

#endif // VIEWEVENTFILTER_H
