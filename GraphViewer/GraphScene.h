#ifndef GRAPHSCENE_H
#define GRAPHSCENE_H

#include <QtWidgets/QGraphicsScene>
#include <QPointer>

#include "Graph.h"

class LegendView;

class GraphScene : public QGraphicsScene
{
		Q_OBJECT

	public:
		explicit GraphScene(QObject* parent = nullptr);

		void addColor(QVariant key);
		void setLegendWidget(LegendView* view);	
		void clear();

	signals:
		void informationMessage(QString);
		void sceneLoaded();

	public slots:
		void saveGraph(QString name);
		void loadGraph(QString fileName);

	private slots:
		void addNode(SharedNode node);
		void addEdge(SharedEdge edge);
		void loadPositions();

	private:
		QString m_name;
		QPointer<Graph> m_graph;
		QMap<SharedElement, QGraphicsItem*> m_items;
		SharedNode m_centralNode;
		int m_modulo;
		int m_placementIndex;
		QMap<QString, QColor> m_colors;
		QColor m_lastColor;
		LegendView* m_legendWidget;
};

#endif // GRAPHSCENE_H
