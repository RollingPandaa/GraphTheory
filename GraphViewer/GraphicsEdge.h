#ifndef GRAPHICSEDGE_H
#define GRAPHICSEDGE_H

#include <GraphicsNode.h>

class GraphicsEdge : public QGraphicsItem
{
	public:
		GraphicsEdge(SharedEdge edge, QGraphicsItem* from, QGraphicsItem* to);
		void setOriented(bool oriented) { m_oriented = oriented; }
		void setColor(QColor color);

		QUuid ident() const { return m_edge->ident(); }
		QRectF boundingRect() const Q_DECL_OVERRIDE;
		QPainterPath shape() const Q_DECL_OVERRIDE;
		void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) Q_DECL_OVERRIDE;


	private:
		void updatePosition();
		QLineF getLine() const;
		QVector<QLine> arrow() const;

	private:
		SharedEdge m_edge;
		QGraphicsItem* m_fromNode;
		QGraphicsItem* m_toNode;
		bool m_oriented;
		QColor m_color;
};

#endif // GRAPHICSEDGE_H
