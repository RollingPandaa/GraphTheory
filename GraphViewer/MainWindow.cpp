#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QTimer>
#include <QFileDialog>
#include <QStyleOption>
#include <QSettings>
#include <QMessageBox>
#include <QDebug>

#include "GraphScene.h"
#include "ViewEventFilter.h"

MainWindow::MainWindow(QWidget *parent):
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	m_legend(nullptr)
{
	ui->setupUi(this);

	ui->graphicsView->setBackgroundBrush(QBrush(QColor::fromRgb(225, 225, 225)));
	ui->graphicsView->installEventFilter(new ViewEventFilter(ui->graphicsView, this));
	m_scene = new GraphScene(this);
	connect(m_scene, &GraphScene::informationMessage, this, &MainWindow::showMessage);

	ui->graphicsView->setScene(m_scene);
	ui->graphicsView->setDragMode(QGraphicsView::RubberBandDrag);
	ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	ui->graphicsView->show();


	QFont f(ui->graphicsView->font());
	f.setPointSize(11);
	ui->graphicsView->setFont(f);
	m_scene->setFont(f);

	m_legend = new LegendView(this);
	m_scene->setLegendWidget(m_legend);

	connect(ui->pushButtonSave, &QPushButton::clicked, this, &MainWindow::saveGraph);
	connect(ui->comboBoxName, &QComboBox::textActivated, m_scene, &GraphScene::loadGraph);
	connect(ui->pushButtonNew, &QPushButton::clicked, m_scene, &GraphScene::clear);
	connect(ui->pushButtonNew, &QPushButton::clicked, this, [this]() { ui->comboBoxName->setCurrentText(""); });
	connect(ui->pushButtonNew, &QPushButton::clicked, m_legend, &LegendView::clear);

	loadNames();
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::paintEvent(QPaintEvent* event)
{
	QMainWindow::paintEvent(event);

	m_legend->resize(ui->graphicsView->width() * 0.3, m_legend->height());
	m_legend->move(ui->graphicsView->x() + ui->graphicsView->width() * 0.7 - 5, ui->graphicsView->y() + 5);
}

void MainWindow::loadNames()
{
	QSettings file(QSettings::IniFormat, QSettings::UserScope, QCoreApplication::organizationName(), QCoreApplication::applicationName());
	ui->comboBoxName->clear();
	ui->comboBoxName->addItem("");
	ui->comboBoxName->addItems(file.childGroups());

	qDebug() << Q_FUNC_INFO << ui->graphicsView->children();
}

void MainWindow::saveGraph()
{
	if(ui->comboBoxName->currentText().isEmpty())
		showMessage(tr("Cannot save a graph without a name"));
	else
		m_scene->saveGraph(ui->comboBoxName->currentText());
}

void MainWindow::showMessage(QString message)
{
	QMessageBox::information(this, tr("Information"), message, QMessageBox::Ok);
}
