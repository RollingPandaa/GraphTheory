QT       += core gui
QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    GraphScene.cpp \
    GraphicsEdge.cpp \
    GraphicsNode.cpp \
    LegendView.cpp \
    ViewEventFilter.cpp \
    main.cpp \
    MainWindow.cpp

HEADERS += \
    GraphScene.h \
    GraphicsEdge.h \
    GraphicsNode.h \
    LegendView.h \
    MainWindow.h \
    ViewEventFilter.h

FORMS += \
    MainWindow.ui

win32:CONFIG(release, debug|release): LIBS += -L$${OUT_PWD}/../LibGraph/release -lLibGraph
else:win32:CONFIG(debug, debug|release): LIBS += -L$${OUT_PWD}/../LibGraph/debug -lLibGraph

INCLUDEPATH += $$PWD/../LibGraph

RESOURCES += \
	resources.qrc
