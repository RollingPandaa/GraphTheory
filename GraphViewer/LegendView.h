#ifndef LEGENDVIEW_H
#define LEGENDVIEW_H

#include <QWidget>
#include <QVariant>

class LegendView : public QWidget
{
	public:
		explicit LegendView(QWidget* parent = nullptr);

		void setColors(QMap<QString, QColor> color);

	public slots:
		void clear();

	protected:
		void paintEvent(QPaintEvent* event) override;

	private:
		void rebuild();

	private:
		QMap<QString, QColor> m_color;
};

#endif // LEGENDVIEW_H
