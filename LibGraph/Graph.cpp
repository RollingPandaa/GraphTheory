#include "Graph.h"

Graph::Graph(QObject* parent):
	QObject(parent)
{
}

Graph::Graph(bool oriented, bool multiple, bool reflexive, QObject *parent) :
	QObject(parent),
	m_oriented(oriented),
	m_multiple(multiple),
	m_reflexive(reflexive),
	m_labelUniq(false)
{
}

Graph::~Graph()
{
	m_edges.clear();
	m_adjacence.clear();
}

quint64 Graph::degree(SharedNode) const
{
	return -1;
}

void Graph::transformToOriented(bool setBidirectionnal)
{
	Q_UNUSED(setBidirectionnal)
}

void Graph::transformToMultiple()
{

}

void Graph::setReflexive(bool reflexive)
{
	Q_UNUSED(reflexive)
}

SharedNode Graph::addNode(QVariant label, qreal weight)
{
	SharedNode n(nodeByLabel(label));
	if(isUniqueLabels() && !n.isNull())
		return n;
	else
	{
		n.reset(new GraphElement(label, weight));
		m_adjacence.insert(n, QMultiMap<SharedNode, SharedEdge>());
		emit nodeAdded(n);
		return n;
	}
}

SharedNode Graph::getNode(QUuid ident) const
{
	SharedNode res(nullptr);

	auto iter = m_adjacence.keyBegin();
	while(res == nullptr && iter != m_adjacence.keyEnd())
	{
		if((*iter)->ident() == ident)
			res = (*iter);
		else
			iter++;
	}

	return res;
}

SharedEdge Graph::addEdge(SharedNode startNode, SharedNode endNode, QVariant label)
{
	if(containsNode(startNode) && containsNode(endNode))
	{
		if(isMultiple() || !hasEdge(startNode, endNode))
		{
			SharedEdge e(new GraphElement(label));
			m_adjacence[startNode].insert(endNode, e);
			m_edges.insert(e, qMakePair(startNode, endNode));
			emit edgeAdded(e);
			return e;
		}
		else if(!isMultiple())
			return m_adjacence.value(startNode).value(endNode);
		else
			return nullptr;
	}
	else
		return nullptr;
}

SharedEdge Graph::getEdge(QUuid ident)
{
	SharedEdge res(nullptr);

	auto iter = m_edges.keyBegin();
	while(res == nullptr && iter != m_edges.keyEnd())
	{
		if((*iter)->ident() == ident)
			res = (*iter);
	}

	return res;
}

SharedNode Graph::getStartNodeOf(SharedEdge edge)
{
	if(m_edges.contains(edge))
		return m_edges.value(edge).first;
	else
		return nullptr;
}

SharedNode Graph::getEndNodeOf(SharedEdge edge)
{
	if(m_edges.contains(edge))
		return m_edges.value(edge).second;
	else
		return nullptr;
}

void Graph::removeNode(SharedNode node)
{
	if(containsNode(node))
	{
		foreach(SharedNode from, m_adjacence.keys())
		{
			foreach(SharedNode to, m_adjacence.value(from).keys())
			{
				if(from == node || to == node)
					removeEdge(from, to);
			}
		}

		m_adjacence.remove(node);
		emit nodeRemoved(node);
	}
}

void Graph::removeEdge(SharedEdge edge)
{
	if(m_edges.contains(edge))
	{
		bool removed(false);
		auto fromIter = m_adjacence.begin();
		while(!removed && fromIter != m_adjacence.end())
		{
			auto toIter = fromIter->begin();
			while(!removed && toIter != fromIter->end())
			{
				if(toIter.value() == edge)
					removeEdge(fromIter.key(), toIter.key());

				toIter++;
			}
			fromIter++;
		}
	}
}

void Graph::removeEdge(SharedNode from, SharedNode to)
{
	if(m_adjacence.contains(from) && m_adjacence.value(from).contains(to))
	{
		m_edges.remove(m_adjacence.value(from).value(to));
		emit edgeRemoved(m_adjacence[from].take(to));
	}
	else if(!isOriented() && m_adjacence.contains(to) && m_adjacence.value(to).contains(from))
	{
		m_edges.remove(m_adjacence.value(to).value(from));
		emit edgeRemoved(m_adjacence[to].take(from));
	}
}

void Graph::removeEdges(QList<SharedEdge> edges)
{
	auto fromIter = m_adjacence.begin();
	while(fromIter != m_adjacence.end() && !edges.isEmpty())
	{
		auto toIter = fromIter->begin();
		while(toIter != fromIter->end() && !edges.isEmpty())
		{
			if(edges.contains(toIter.value()))
				removeEdge(fromIter.key(), toIter.key());

			toIter++;
		}
		fromIter++;
	}
}

bool Graph::containsNode(SharedNode node)
{
	return m_adjacence.contains(node);
}

bool Graph::containsEdge(SharedEdge edge)
{
	return m_edges.contains(edge);
}

bool Graph::hasEdge(SharedNode from, SharedNode to)
{
	return m_adjacence.contains(from) && m_adjacence.value(from).contains(to);
}

SharedNode Graph::nodeByLabel(QVariant label)
{
	SharedNode n(nullptr);
	auto it = m_adjacence.keyBegin();
	while(it != m_adjacence.keyEnd())
	{
		if((*it)->label == label)
		{
			n = (*it);
			it = m_adjacence.keyEnd();
		}
		else
			it++;
	}
	return n;
}

void Graph::saveGraph(QSettings& file)
{
	file.setValue("oriented", m_oriented);
	file.setValue("multi", m_multiple);
	file.setValue("reflex", m_reflexive);
	file.setValue("labeled", m_labelUniq);

	int i(0);
	file.beginWriteArray("nodes");
	foreach(SharedNode node, m_adjacence.keys())
	{
		file.setArrayIndex(i++);
		file.setValue("ident", node->ident());
		file.setValue("label", node->label);
		file.setValue("weight", node->weight);
	}
	file.endArray();

	i = 0;
	file.beginWriteArray("edges");
	foreach(SharedNode from, m_adjacence.keys())
	{
		foreach(SharedNode to, m_adjacence.value(from).keys())
		{
			file.setArrayIndex(i++);
			SharedEdge edge(m_adjacence.value(from).value(to));

			file.setValue("ident", edge->ident());
			file.setValue("label", edge->label);
			file.setValue("weight", edge->weight);
			file.setValue("from", from->ident());
			file.setValue("to", to->ident());
		}
	}
	file.endArray();
}

void Graph::loadGraph(QSettings& file)
{
	m_edges.clear();
	m_adjacence.clear();

	m_oriented = file.value("oriented", false).toBool();
	m_multiple = file.value("multi", false).toBool();
	m_reflexive = file.value("reflex", false).toBool();
	m_labelUniq = file.value("labeled", false).toBool();

	int size(file.beginReadArray("nodes"));
	for(int i = 0; i < size; ++i)
	{
		file.setArrayIndex(i);
		SharedNode node(new GraphElement(file.value("label"), file.value("weight").toReal(), file.value("ident").toUuid()));
		m_adjacence.insert(node, QMultiMap<SharedNode, SharedEdge>());
	}
	file.endArray();

	size = file.beginReadArray("edges");
	for(int i = 0; i < size; ++i)
	{
		file.setArrayIndex(i);
		SharedEdge edge(new GraphElement(file.value("label"), file.value("weight").toReal(), file.value("ident").toUuid()));
		SharedNode from(getNode(file.value("from").toUuid()));
		SharedNode to(getNode(file.value("to").toUuid()));
		m_adjacence[from].insert(to, edge);
		m_edges.insert(edge, qMakePair(from, to));
	}
	file.endArray();

	foreach(SharedNode n, m_adjacence.keys())
		emit nodeAdded(n);

	foreach(SharedEdge e, m_edges.keys())
		emit edgeAdded(e);

	emit graphLoaded();
}
