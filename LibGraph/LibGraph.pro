#-------------------------------------------------
#
# Project created by QtCreator 2015-06-02T17:52:39
#
#-------------------------------------------------

QT       -= gui

TARGET = LibGraph
TEMPLATE = lib

DEFINES += LIBGRAPH_LIBRARY

SOURCES += GraphTheory.cpp \
    Graph.cpp \
    GraphElement.cpp

HEADERS += GraphTheory.h\
	GraphElement.h \
        libgraph_global.h \
    Graph.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
