#ifndef GRAPHELEMENT_H
#define GRAPHELEMENT_H

#include "libgraph_global.h"

#include <QObject>
#include <QUuid>
#include <QVariant>
#include <QSharedPointer>

class GraphElement;
typedef QSharedPointer<GraphElement> SharedElement;
typedef SharedElement SharedNode;
typedef SharedElement SharedEdge;

class LIBGRAPHSHARED_EXPORT GraphElement
{
	public:
		//	==========	Atributes	==========
		QVariant label;
		qreal weight;

	public:
		GraphElement(QVariant label = QVariant(), qreal weight = 0, QUuid id = QUuid::createUuid());
		//		GraphNode(const GraphNode &other);

		QUuid	ident() const { return m_ident; }

		//	==================	Tools		======================
		bool operator ==(const GraphElement& other) { return m_ident == other.m_ident; }
		bool operator ==(const SharedElement& other) { return m_ident == other->m_ident; }

		SharedElement duplicate() const;

	private:
		QUuid m_ident;

		//	Forbid copy
	private:
		Q_DISABLE_COPY(GraphElement)
};

#endif // GRAPHELEMENT_H
