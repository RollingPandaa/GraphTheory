#include "GraphElement.h"

GraphElement::GraphElement(QVariant label, qreal weight, QUuid id):
	m_ident(id),
	label(label),
	weight(weight)
{
}

SharedElement GraphElement::duplicate() const
{
	return SharedElement(new GraphElement(label, weight));
}
