#ifndef GRAPH_H
#define GRAPH_H

#include <QObject>
#include <QSettings>

#include "GraphElement.h"

class LIBGRAPHSHARED_EXPORT Graph : public QObject
{
		Q_OBJECT
	public:
		explicit Graph(QObject* parent = nullptr);
		Graph(bool oriented = false, bool multiple = false, bool reflexive = false, QObject *parent = 0);
		~Graph();

		bool isOriented() const { return m_oriented; }
		bool isMultiple() const { return m_multiple; }
		bool isReflexive() const { return m_reflexive; }
		bool isUniqueLabels() const { return m_labelUniq; }
		void setUniqueLabels(bool unique) { m_labelUniq = unique; }

		quint64 order() const { return m_adjacence.size(); }
		quint64 size() const { return m_edges.size(); }
		quint64 degree(SharedNode) const;

		//	======	Can be heavy	======
		void transformToOriented(bool setBidirectionnal = false);	//	If setBidirectionnal, non-oriented edges are transformed in 2 edges of opposite directions
		void transformToMultiple();
		void setReflexive(bool reflexive);	//	If transform to not reflexive, reflexive edges are removed

		SharedNode addNode(QVariant label = QVariant(), qreal weight = 0);
		SharedNode getNode(QUuid ident) const;
		QList<SharedNode> getNodes() const { return m_adjacence.keys(); }

		SharedEdge addEdge(SharedNode startNode, SharedNode endNode, QVariant label = QVariant());
		SharedEdge getEdge(QUuid ident);
		QList<SharedEdge> getEdges() const { return m_edges.keys(); }
		SharedNode getStartNodeOf(SharedEdge edge);
		SharedNode getEndNodeOf(SharedEdge edge);

		void removeNode(SharedNode node);
		void removeEdge(SharedEdge edge);
		void removeEdge(SharedNode from, SharedNode to);
		void removeEdges(QList<SharedEdge> edges);

		inline bool containsNode(SharedNode node);
		inline bool containsEdge(SharedEdge edge);
		inline bool hasEdge(SharedNode from, SharedNode to);
		SharedNode nodeByLabel(QVariant label);

		void saveGraph(QSettings& file);
		void loadGraph(QSettings& file);

	signals:
		void nodeAdded(SharedNode n);
		void edgeAdded(SharedEdge e);
		void nodeRemoved(SharedNode n);
		void edgeRemoved(SharedEdge e);
		void graphLoaded();

	private:
		bool m_oriented;
		bool m_multiple;
		bool m_reflexive;
		bool m_labelUniq;
		QMap<SharedEdge, QPair<SharedNode, SharedNode> > m_edges;
		QMap<SharedNode, QMultiMap<SharedNode, SharedEdge> > m_adjacence;
};

#endif // GRAPH_H
